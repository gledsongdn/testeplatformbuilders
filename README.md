# Teste PlatformBuilders

#### Executar BD Docker
Rode na linha de comando para iniciar o banco de dados
```shell
docker-compose up
```

### Executar aplicação
Para iniciar o projeto rode na sua IDE a classe main()
```shell
@SpringBootApplication public class GerensysDevFinalApplication
```

#### Postman
Arquivo postman está na pasta raiz do projeto.  
[Cliente - API.postman_collection.json](./Cliente-API.postman_collection.json)
